import cta from "./cta";
import testimonial from "./testimonial";
import filterableContent from "./filterable-content";
import "../styles/application.scss";

cta();
testimonial();
filterableContent();
