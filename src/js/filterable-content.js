import data from "./data/data.json";

import generateDropdown from "./filterable-content/dropdowns";
import generateResults from "./filterable-content/results";
import filterData from "./filterable-content/filters";

// 1. Get the media data
// 2. Map the data from the media to a proper structure
// 3. Create dropdowns from the mapped data
// 4. Add listeners for when the filters are manipulated
// 5. Generate the html to show the media to a user

export default () => {
  const { media } = data;
  // Return an array of unique genres
  const genres = new Set(
    media
      .map(record => [...record.genre])
      .reduce((a, b) => a.concat(b), [])
      .sort()
  );
  generateDropdown(document.getElementById("genreDropdown"), genres, "genre");

  // Return an array of unique years
  const years = new Set(
    media
      .map(record => parseInt(record.year))
      .reduce((a, b) => a.concat(b), [])
      .sort((a, b) => a - b)
      .map(year => `${year}`)
  );
  generateDropdown(document.getElementById("yearDropdown"), years, "year");

  // Add event listener to every input to filter results when changed
  const inputsContainer = document.getElementById("filters");
  const inputs = inputsContainer.querySelectorAll("input");
  Array.from(inputs).map(input =>
    ["change", "input"].forEach(eventType => {
      input.addEventListener(eventType, event => {
        generateResults(filterData(media));
      });
    })
  );

  // Clear inputs when clicking on clearLink
  document.getElementById("clearLink").addEventListener("click", event => {
    event.preventDefault();

    inputsContainer
      .querySelectorAll('input[type="text"]')
      .forEach(input => (input.value = ""));

    inputsContainer
      .querySelectorAll("input:checked")
      .forEach(input => (input.checked = false));

    generateResults(filterData(media));
  });

  // Generate results on initial load using
  // whatever the values the filters are defaulted to
  generateResults(filterData(media));
};
