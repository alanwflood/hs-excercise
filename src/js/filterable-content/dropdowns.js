// Add focus/unfocus events to dropdown
function addDropdownEvent(el) {
  el.tabIndex = 0; // Makes the dropdown div focusable
  const dropdown = el.querySelector(".dropdown__options");
  // On focus gain
  el.addEventListener(
    "focus",
    event => {
      el.classList.add("open");
      dropdown.style.top = el.getBoundingClientRect().height;
    },
    true
  );
  // On focus loss
  el.addEventListener("focusout", event => {
    // Only close if focused element is outside of the current element
    if (!dropdown.contains(event.relatedTarget)) el.classList.remove("open");
  });
}

// Takes a containing element, a dataset and a prefix (name value)
// and creates a dropdown of checkboxes
function mapDataToDropdown(parentElement, dataToMap, prefix) {
  const optionsContainer = parentElement.querySelector(".dropdown__options");

  dataToMap.forEach(record => {
    const option = document.createElement("label");
    const uid = `${prefix}_${record}`;
    option.classList.add("option");
    option.htmlFor = uid;
    option.innerHTML = `<input type="checkbox" name="${prefix}" value="${record}" id="${uid}"/><div>${record}</div>`;
    optionsContainer.appendChild(option);
  });

  addDropdownEvent(parentElement);
}

export default mapDataToDropdown;
