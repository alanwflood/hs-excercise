// Capitalize the first letter of a string
function titleizeString(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

// Generate a result element from a data record
function generateResultHTML(record) {
  const { poster, title, years, genre, type } = record;
  const genreString = genre.map(genre => titleizeString(genre)).join(", ");
  const recordParent = document.createElement("li");

  recordParent.innerHTML = `<div style="background-image: url('${poster}');" class="poster"></div><div class="details"><div>${title}</div><div>Genres: ${genreString}</div></div>`;

  return recordParent;
}

function generateEmptyHTML() {
  const recordParent = document.createElement("li");
  recordParent.classList.add("empty");
  recordParent.innerHTML = "<div>No Media found!</div>";
  return recordParent;
}

// Convert data into valid html and append to the page
function mapDataToHTML(data) {
  const parent = document.getElementById("results");
  parent.innerHTML = ""; // Reset the container
  // Sort the data alphabetically
  const dataElements = data.sort((a, b) => a.title.localeCompare(b.title));
  if (!!dataElements.length) {
    dataElements.forEach(record =>
      parent.appendChild(generateResultHTML(record))
    );
  } else {
    // No media found
    parent.appendChild(generateEmptyHTML());
  }
}

export default mapDataToHTML;
