import fuzzySearch from "fuzzysearch";

// Get value of every input
function filterInputValues(selector) {
  return (
    Array.from(document.getElementById("filters").querySelectorAll(selector))
      // Map key value pairs from inputs
      .map(input => [input.name, input.value])
      // Strip out pairs with empty keys or values
      .filter(
        ([key, value]) => (!!value || !!value.length) && (!!key || !!key.length)
      )
  );
}

function filterDataFromInputValues(data) {
  // These values only check if any of them are matched
  const looseSearchArray = filterInputValues("input[type=checkbox]:checked");

  // These values enforce that they are matched
  const strongSearchArray = filterInputValues(
    "input[type=radio]:checked, input[type=text]"
  );

  return data.filter(obj => {
    // Check for any matches for loose values
    const looseFilter =
      !looseSearchArray.length ||
      looseSearchArray.some(([key, value]) => obj[key].includes(value));

    // Check for exact matches for strong values
    const strongFilter =
      !strongSearchArray.length ||
      strongSearchArray.every(
        ([key, value]) =>
          // if using title search go by a fuzzy string search which returns a boolean
          // else just check inclusion
          key === "title"
            ? fuzzySearch(value.toLowerCase(), obj[key].toLowerCase())
            : obj[key].includes(value)
      );

    return looseFilter && strongFilter;
  });
}

export default filterDataFromInputValues;
