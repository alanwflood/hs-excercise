import loader from "../assets/svgs/loading.svg";

function createLoader(parentEl) {
  parentEl.innerHTML = loader;
}

function ctaLinkClick() {
  const ctaQuoteEl = document.getElementById("ctaQuote");
  const ctaLink = document.getElementById("ctaLink");
  fetchJoke();

  ctaLink.addEventListener("click", event => {
    event.preventDefault();
    fetchJoke();
  });

  // Enable/Disable button
  function disableLink(bool) {
    ctaLink.style.pointerEvents = bool ? "none" : "auto";
    ctaLink.style.cursor = bool ? "default" : "pointer";
  }

  // Fetch text from url
  function fetchJoke() {
    const url = "https://icanhazdadjoke.com/";
    createLoader(ctaQuoteEl);
    disableLink(true);

    fetch(url, {
      headers: {
        accept: "application/json"
      }
    })
      .then(res => res.json())
      .then(j => {
        ctaQuoteEl.innerHTML = "";
        ctaQuoteEl.innerText = j.joke;
        disableLink(false);
      })
      .catch(err => console.error(err));
  }
}

export default ctaLinkClick;
