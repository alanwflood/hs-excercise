import faker from "faker"; // Library to generate fake data
import loader from "../assets/svgs/loading.svg";

function createLoader(parentEl) {
  parentEl.innerHTML = loader;
}

function setText(text, el) {
  el.innerHTML = "";
  el.innerText = text;
}

function getTestimonialQuote() {
  const url = "https://baconipsum.com/api/?type=meat-and-filler";
  const el = document.getElementById("testimonialText");
  createLoader(el);
  fetch(url)
    .then(res => res.json())
    .then(data => setText(data[1], el))
    .catch(err => console.error(err));
}

function getTestimonialAuthor() {
  const el = document.getElementById("testimonialAuthor");
  setText(faker.name.findName(), el);
}

export default () => {
  (function() {
    getTestimonialQuote();
    getTestimonialAuthor();
  })();
};
