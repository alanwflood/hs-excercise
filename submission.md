# Submission Notes 

These notes will be read by HubSpot developers. Drop us a line!

## Given more time, what would you have done differently?

Idealy with more time I would have:

* Done more cross browser/device testing
* Added proper meta tags in the head of the page
* Moved the pages components into react components
* Ran through the javascript and sass with a linter
* Made a small production server so the site could be hosted in a production environment and not just through webpack's dev-server
* Most likely refactored some SCSS and JS files to save on filesize

## Is there anything else you'd like to let us know?

The challenge was a lot of fun

The filters really required some critical thinking about their logic which added a lot more bulk to the final task but it was fun to get it working.

The requirement for abiding by the html elements on the page was also interesting as it required some css magic to get it as close to the designs as possble.

All in all, I hope you like the end results!
