const webpack = require("webpack");
const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const merge = require("webpack-merge");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const baseConfig = require("./base.config.js");

module.exports = merge(baseConfig, {
  output: {
    path: path.join(__dirname, "../build"),
    publicPath: "/",
    filename: "[name].bundle.[chunkhash].js"
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: ["css-loader"]
        })
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: ["css-loader", "sass-loader"]
        })
      }
    ]
  },

  plugins: [
    // Extract imported CSS into own file
    new ExtractTextPlugin("[name].bundle.[contenthash].css"),
    // Minify JS
    new UglifyJsPlugin({
      sourceMap: false,
      uglifyOptions: {
        compress: true
      }
    }),
    // Minify CSS
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ]
});
