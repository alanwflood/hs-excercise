const webpack = require("webpack");
const path = require("path");
const htmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: {
    app: [path.join(__dirname, "../src/js/index.js")]
  },

  module: {
    rules: [
      {
        test: /\.js/,
        exclude: /node_modules/,
        use: "babel-loader"
      },
      {
        test: /\.ejs$/,
        use: {
          loader: "ejs-compiled-loader"
        }
      },
      {
        test: /\.json$/,
        use: "json-loader"
      },
      {
        test: /\.svg$/,
        use: "raw-loader"
      }
    ]
  },

  plugins: [
    new webpack.EnvironmentPlugin(["NODE_ENV"]),
    new htmlWebpackPlugin({
      title: "HTML Webpack Plugin",
      inject: true,
      template: "!!ejs-compiled-loader!src/views/pages/index.ejs"
    })
  ],

  resolve: {
    extensions: [".js", ".ejs"]
  }
};
